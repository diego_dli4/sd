'use strict'

const port = process.env.PORT || 3000;   //Conectamos con el puerto

const https = require('https');
const fs = require('fs');
const OPTIONS_HTTPS = {
    key: fs.readFileSync('./cert/key.pem'),
    cert: fs.readFileSync('./cert/cert.pem')
}

const cors = require('cors');
const express = require('express');
const logger = require('morgan');
const mongojs = require('mongojs');

const app = express();

var db = mongojs("SD");         //Enlazamos con la BD 'SD'
var id = mongojs.ObjectID;      //Convertimos un id a ObjectID

var allowMethods = (req, res, next) => {
    res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
    return next();
};
var allowCrossTokenHeader = (req, res, next) => {
    res.header("Access-Control-Allow-Headers", "*");
    return next();
};
var allowCrossTokenOrigin = (req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    return next();
};
var auth = (req, res, next) => {
    if(req.headers.token === "password1234") {
        return next();
    } else {
        return next(new Error("No autorizado"));
    };
};

// midelwares
app.use(logger('dev'));
app.use(express.urlencoded({extended: false}));
app.use(express.json());

app.use(cors());
app.use(allowMethods);
app.use(allowCrossTokenHeader);
app.use(allowCrossTokenOrigin);

// Añadimos un trigger para dar soporte a las coleciones
app.param("coleccion", (req, res, next, coleccion) => {
    console.log('param /api/:coleccion');
    console.log('colección: ', coleccion);

    req.collection = db.collection(coleccion);
    return next();
});

//Declaramos las rutas del servicio junto a sus controladores y lógica de negocio

// Para obtener todas las colecciones existentes en la BD
app.get('/api', (req, res, next) => {
    console.log('GET /api');
    console.log(req.params);
    console.log(req.collection);

    db.getCollectionNames((err, colecciones) => {
        if (err) return next(err);
        res.json(colecciones);
    });
});

// Para obtener los elementos de la tabla coleccion
app.get('/api/:coleccion', (req, res, next) => {
    req.collection.find((err, coleccion) => {
        if (err) return next(err);
        res.json(coleccion);
    });
});

// Para obtener el elemento indicado de la tabla coleccion
app.get('/api/:coleccion/:id', (req, res, next) => {
    req.collection.findOne({_id: id(req.params.id)}, (err, elemento) => {
        if (err) return next(err);
        res.json(elemento);
    });
});

// Creamos un nuevo elemento en la tabla coleccion
app.post('/api/:coleccion', auth, (req, res, next) => {
    const elemento = req.body;

    if(!elemento.nombre){
        res.status(400).json ({
            error: 'Bad data',
            description: 'Se precisa al menos un campo <nombre>'
        });
    }
    else{
        req.collection.save(elemento, (err, coleccionGuardada) => {
            if(err) return next(err);
            res.json(coleccionGuardada);
        });
    }
});

// Actualizamos un elemento de la tabla coleccion
app.put('/api/:coleccion/:id', auth, (req, res, next) => {
    let elementoId = req.params.id;
    let elementoNuevo = req.body;
    req.collection.update({_id: id(elementoId)},
        {$set: elementoNuevo}, {save: true, multi: false}, (err, elementoModif) => {
            if (err) return next(err);
            res.json(elementoModif);
    });
});

// Eliminamos un elemento de la tabla coleccion
app.delete('/api/:coleccion/:id', auth, (req, res, next) => {
    let elementoId = req.params.id;
   
    req.collection.remove({_id: id(elementoId)}, (err, resultado) => {
        if(err) return next(err);
        res.json(resultado);
    });
});

https.createServer(OPTIONS_HTTPS, app).listen(port, () => {
    console.log(`SEC WS API REST CRUD con DB ejecutandose en https://localhost:${port}/api/:coleccion/:id`);
});