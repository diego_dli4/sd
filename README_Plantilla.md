# Backend CRUD API REST

Ejemplo de WS REST con NodeJS que proporciona un API CRUD para gestionar una DB MongoDB.
Realizado por Diego Lanuza Izquierdo DNI 26277123Z Identificador: dli4

## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._

Ver **Deployment** para conocer cómo desplegar el proyecto.


### Pre-requisitos 📋

Vamos a trabajar con Ubuntu de Linux, version 20.04 LTS de 64 bits, mínimo de 2GHz de Procesador, 4GB de RAM y 25GB de HD.
Que recursos necesitamos y como los instalamos.

Instalaremos todo mediante la terminal, se abre mediante <Ctrl+Alt+T>

-Visual Studio Code
$ sudo snap install --classic code

-Postman
$ sudo snap install postman

-NodeJS
1.Instalamos el gestor de paquetes de Node (npm)
$ sudo apt update
$ sudo apt install npm
2.Instalamos una utilidad de NodeJs llamada n
$ sudo npm clean -f
$ sudo npm i -g n
3.Instalamos NodeJS
$ sudo n stable

-Gestor de repositorios git
$ sudo apt install git
$ git config --global user.name dli4
$ git config --global user.email dli4@alu.ua.es
$ git config --list

-Express
$ npm i -S express

-Nodemon y Morgan
(lo instalaremos en la carpeta que estamos trabajando)
1.$ cd node/api-rest/
2.$ npm i -D nodemon
3.$ npm i -S morgan

-MongoDB
$ sudo apt update
$ sudo apt install -y mongodb
Y para instalar bibliotecas de mongo
$ cd node/api-rest/
$ npm i -S mongodb
$ npm i -S mongojs

### Instalación 🔧

-Lo primero que debemos hacer es conectarnos a un repositorio externo para ir subiendo nuestro trabajo a medida que lo vamos actualizando.
Creamos el repositorio local y lo conectamos:
$ git init
$ git remote -v
$ git remote add origin https://dli4@bitbucket.org/dli4/api-rest.git
Ahora podemos subir versiones de nuestro trabajo mediante:
$ git add .
$ git commit -m "Que hemos cambiado"
$ git push
Tambien podemos añadir un tag para que se guarden los archivos
$ git tag v1.0.0
$ git push --tags
(Tendremos que actualizar la version en el archivo package.json)


-Para empezar nuestro proyecto utilizaremos npm mediante:
$ npm init
Y rellenamos las cuestiones que nos solicitan


-Mediante VisualStudio tendremos que crear un archivo index.js, en el cual vamos a conectar nuestro código a una base de datos.
Nuestro código tendrá las instrucciones necesarias para leer, publicar, modificar y borrar elementos de nuestra base de datos en MongoDB, mediante los códigos get, post, put y delete.

Además necesitamos conectarnos al puerto, a la base de datos, conectar las aplicaciones que vamos a utilizar, añadir midelwares y un trigger para dar soporte a las colecciones.

Para probarlo nos introducimos en la carpeta en la que estamos trabajando y lo introducimos: 
$ cd node/api-rest/
$ node index.js

Ahora tenemos abierto el puerto y podemos probar si funciona desde cualquier explorador web o mediante Postman mediante la url: http://localhost:3000 ,3000 es el número del puerto que hemos seleccionado previamente, funcionaría con cualquier otro número.


-Para invocar Nodemon, también en VisualStudio en el archivo package.json deberemos incluir la linea de código: "start": "nodemon index.js"
Con este cambio Nodemos nos permite no tener que detener el anterior servidor y volver a lanzar una nueva versión, el texto del servidor se actualiza automáticamente.


-Morgan nos ayuda a verificar en tiempo real cómo está funcionando todo, cada vez que lanzemos el servidor y le hagamos una consulta, gracias a Morgan nos devolverá un mensaje en la terminal con la petición que hemos hecho, un código de mensaje que nos dirá si ha ido bien o no, ej: 200, 400, 500... y el tiempo que ha tardado en realizar la consulta.

-Mediante Postman crearemos unos archivos GET, POST, PUT y DELETE, en el que debemos nombrar a cada uno, añadirle una url.
Para GET y DELETE como solo borramos o mostramos datos, lo único que debemos hacer es ir a las cabezeras 'Headers' y añadir en Key: Content-Type y en Value: application/json.
Pero en PUT y POST como introducen y actualizan datos debemos ir a Body, elegir el formato raw/JSON y escribir lo que deseamos incorporar o actualizar a la base de datos, en este caso se realiza el paso anterior automáticamente.
Cuando tengamos toda la colección lo exportamos y la metemos en nuestra carpeta.

## Ejecutando las pruebas ⚙️

Después de hacer el apartado anterior podemos abrir el cliente de mongo en una nueva terminal y comprobar los datos de que hemos introducido, actualizado, borrado.
$ mongo
>show dbs
>use SD //cambiamos a la base de datos
>show collections //nos enseña las tablas que hay
>db.familia.find() //Nos muestra los datos de la tabla familia
>db.familia.find().pretty() //Para que se vean los datos mejor visualmente

### Analice las pruebas end-to-end 🔩

Estas pruebas verifican que estamos modificando la base de datos, añadiendo tablas, introduciendo datos, modificándolos y borrandolos.

Podemos hacer una llamada post a familia añadiendo los datos de un individuo y veremos como se han creado los datos de este en la tabla individuo.

## Construido con 🛠️

* [Bitbucket](https://bitbucket.com) - El repositorio
* [Dropwizard](http://www.dropwizard.io/1.0.2/docs/) - El framework web usado
* [Maven](https://maven.apache.org/) - Manejador de dependencias
* [ROME](https://rometools.github.io/rome/) - Usado para generar RSS

## Autores ✒️

_Menciona a todos aquellos que ayudaron a levantar el proyecto desde sus inicios_

* **Paco Maciá** - *Trabajo Inicial* - [pmacia](https://github.com/pmacia)
